Syntax Nodes
======================


-----------------
1. Nodes
-----------------

A node is a data structure that represents
a syntactical feature in code. It MAY form
a tree-like structure.

To do so, it may sport:

* A type identifier;

* A value (or list of), of one of the following kinds:

  a) A boolean value (`boolValue`),
  b) An integer numerical value (`intValue`),
  c) A (single or double) floating-point numerical value (`floatValue`),
  d) A string value (`strValue`),
  e) A string containing an identifier name (`identValue`),
  f) A `"type"` node (`typeValue`), and/or
  g) Any node (`nodeValue` - only used to store node values like functions or otherwise nodes in general).

* A single reference to a node (`body`);

* A list of children, list of references to nodes (`children`).


Types MIGHT not use all of the fields.  If all fields
must be present in the implementation (e.g. a language
that does not support C-like unions or any similar construct),
ignored fields SHOULD be set to a default value, e.g. 0
for numerical values.

A statement is usually either an expression (including
function definitions) or a logic control node (if, while,
etc.).


-----------------
2. Node Types
-----------------

A list of types of nodes that MUST be implemented by a Fre
VM follows.

       
a) `"logic.if"`
    'if' block.

    * `children`:
        * Condition expression .
        * *(optional)* 'else' (`"logic.else"`) node.
          
    * `body`:
        * Statement or block.
       
b) `"logic.else"`
    'else' block.

    * `body`: Statement or block.
       
c) `"logic.while"`
    'while' block.

    * `children`:
        * Condition expression.
        * *(optional)* 'else' (`"logic.else"`) node (when no iteration ensued). Defaults to None.
         
    * `body`: Statement or block.
      
d) `"logic.foreach"`
    **Note: this node type is reserved, but UNUSED.**

    'foreach' block.

    * `identValue`: Loop variable name.
         
    * `children`:
        * List(s) to iterate.
        * *(optional)* 'else' (`"logic.else"`) node (when no iteration ensued). Defaults to None.
         
    * `body`: Statement or block.
       
e) `"block"`
    Statement block.

    * `children`:    
        * One or more statements.
       
f) `"expr.funcall"`
    Function call.

    * `identValue`: The function to call.
         
    * `children`: Function call arguments.
       
g) `"expr.identifier"`
    Identifier; variable or function reference.

    * `identValue`: The identifier.
       
h) `"expr.literal.string"`
    String literal.

    * `strValue`: Literal value.
       
i) `"expr.literal.int"`
    Integer literal.

    * `intValue`: Literal value.
       
j) `"expr.literal.float"`
    Floating point literal.

    * `floatValue`: Literal value.
       
k) `"expr.literal.value"`
    Value literal.

    This one is a bit complicated. Each implementation has
    their own way to separate identifiers, number/string literals 
    and *value literals*. The latter are values that are always
    the same when their name is found, similarly to an identifier.

    e.g. in the Python implementation, true would be "True", and null would be "None".

    Therefore, depending on the kind of the value, the node MUST
    store the value accordingly.

    e.g. `boolValue` for true/false, no value for null, etc.

    The only constraint is that the VM's interpreter can understand
    and use the value as any other value.
       
l) `"expr.oper"`
    Operation (uses polish notation, similarly to Lisp).

    * `strValue`: Operator (e.g. "+" for addition).
         
    * `children`: Operands.
       
m) `"expr.func"`
    Function definition.

    * List of values.
        * `identValue`: Function name (can be null for anonymous functions).
         
    * `children`:
        * Function body.
        * Arguments. (see node type `"expr.argdef"`)
         
    * `body`: Return type as a `"type"` node.

n) `"expr.argdef"`
    Function argument.

    * `identValue`: Argument name.
    
    * `body`: Argument type. (see node type `"type"`)

o) `"expr.assign"`
    **Obsoleted; see `"expr.set"`.**

p) `"return"`
    This statement would return a value from a function.

    * `body`: Node of expression or value that would be returned (expression).

q) `"continue"`
    Continues to the next iteration in a while loop.

r) `"break"`
    Breaks a while loop.

s) `"pass"`
    A statement that does nothing.

t) `"expr.set"`
    A statement that sets an existing variable to a value. Can also be used as an expresion.

    * `identValue`: Identifier to set to.

    * `body`: Expression this identifier would be set to.

u) `"type"`
    A type node.

    * `strValue`: The type.

v) `"identifier"`
    An identifier name. Used to represent an identifier as a node instead of a string/`identValue`.

    * `identValue`: The identifier name represented by this node.

w) `"vardef"`
    A variable definition.

    * Values:
        * `identValue`: The identifier name of this variable.
        * (optional) any: The value of this variable. (defaults to None)

    * Body: the `"type"` of this variable.