Fre
=========


Short for

* **F** lexible
* **Re** adability,

Fre is a simple, easily parseable and readable, but at the same time
flexible, strong-typed, and interpreted programming language.

Its original intent was to provide ZDoom (a game engine based on Doom)
a language that could be parsed by its existing ZScript language. However,
a Python VM/interpreter was made instead, given a better estimation of
the complexity of the project.

Running Scripts
----------------

```
$ python3 -m fre < myCode.fre
```

See the `examples/` folder for examples.

Technical Documentation
------------------------

See the `docs/` folder for technical information regarding parsing.
